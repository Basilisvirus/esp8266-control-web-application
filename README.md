# ESP8266-control-web-application

Control ESP8266 pins using a web application, locally

Esp simply on breadboard:

[![esp-1.png](https://i.postimg.cc/k57DsFcv/esp-1.png)](https://postimg.cc/dhSqtCgh)

My web interface:

[![esp.png](https://i.postimg.cc/Dwr81Yqz/esp.png)](https://postimg.cc/ZWqbSHsk)
